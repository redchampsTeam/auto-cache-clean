This is a simple Magento 2 extension to clean invalidated cache automatically. While working in Magento admin, you may have observed that message "One or more of the Cache Types are invalidated" is popping up frequently when you change something. It means that one of cache needs to be cleaned at admin path 'System > Cache Management'. Now you have to visit that section and manually select and clean those invalidated caches.

So we developed this extension to clean those caches automatically.

## Installation

Execute below two commands in Magento 2 root

1. **composer config repositories.redchamps composer https://repo.redchamps.com**
2. **composer require redchamps/module-auto-cache-clean**