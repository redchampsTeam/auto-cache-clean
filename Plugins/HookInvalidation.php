<?php
/**
 * Created by RedChamps.
 * User: Rav
 * Date: 29/08/17
 * Time: 2:01 PM
 */
namespace RedChamps\AutoCacheClean\Plugins;

class HookInvalidation
{
    public function aroundInvalidate(\Magento\Framework\App\Cache\TypeList $subject, callable $proceed, $typeCode)
    {
        if(is_array($typeCode)) {
            foreach ($typeCode as $type) {
                $subject->cleanType($type);
            }
        } else {
            $subject->cleanType($typeCode);
        }
    }
}